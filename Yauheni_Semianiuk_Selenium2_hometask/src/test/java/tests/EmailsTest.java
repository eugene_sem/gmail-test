package tests;
import forms.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class EmailsTest extends BaseTest {

    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                // fill in test data according to following conditions
                // new Object[] {"user1 login","user1 password","user1 email address","user2 email address", "email topic", "email content"}
                new Object[]{"es.test.acc1", "qwe123QWE", "es.test.acc1@gmail.com", "es.test.acc2@gmail.com", RandomGenerator.makeRandomString(10), "Email Content"}
        };
    }

    @Test(dataProvider = "testData")
    public void testSaveEmailToDraftsAndSendIt(String user1Login, String user1Password, String user1Email, String user2Email, String topic, String emailContent) {
        driver.get("http://gmail.com/");
        EnterEmailPage emailPage = new EnterEmailPage();
        EnterPasswordPage passwdPage = emailPage.typeUsername(user1Login).submitEmail();
        InboxFolder inbox = passwdPage.typePassword(user1Password).signIn();
        NewDraftEmail newDraftEmail = inbox.clickWriteMessageButton();
        newDraftEmail.enterAddressee(user2Email)
                .enterTopic(topic)
                .enterEmailContent(emailContent)
                .saveAndCloseMessage();
        DraftFolder drafts = inbox.goToDraftFolder();
        Assert.assertTrue(drafts.isMessageFound(topic));
        DraftEmail openedDraft = drafts.openDraftEmail(topic);
        Assert.assertEquals(openedDraft.getAddressee(), user2Email);
        Assert.assertEquals(openedDraft.getEmailTopic(), topic);
        Assert.assertEquals(openedDraft.getEmailContent(), emailContent);
        openedDraft.sendMessage();
        DraftFolder drafts2 = new DraftFolder();
        Assert.assertFalse(drafts2.isMessageFound(topic));
        SentFolder sent = drafts2.goToSentFolder();
        Assert.assertTrue(sent.isMessageFound(topic));
        AccountInformation accountInfo = sent.openAccountInfo();
        EnterPasswordPage passwdPage2 = accountInfo.logOut();
    }
}


