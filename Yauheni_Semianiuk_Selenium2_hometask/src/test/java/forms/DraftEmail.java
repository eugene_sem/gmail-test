package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import java.util.logging.Logger;

public class DraftEmail extends MainPage {
    private static final Logger log = Logger.getLogger(DraftEmail.class.getName());
    private By addresseeLocator = By.cssSelector("div[tabindex='1'] > div > span[email]");
    private By topicLocator = By.cssSelector("td > div > h2 > div[style]");
    private By emailContentLocator = By.cssSelector("td > div > div[role='textbox']");
    private By sendButtonLocator = By.cssSelector("td > div > div[role='button']");
    protected static By closeButtonLocator = By.cssSelector("img[src='images/cleardot.gif'][data-tooltip-delay='800']");
    private static By deleteDraftButtonLocator = By.cssSelector("td > div > div > div[tabindex='1']");


    protected final WebDriver driver;
    public DraftEmail(){
        super(webDriver, deleteDraftButtonLocator);
        this.driver = webDriver;
    }
    public DraftEmail enterAddressee(String addressee){
        log.info("Enter Addressee: " + addressee);
        driver.findElement(addresseeLocator).sendKeys(addressee);
        return this;
    }
    public DraftEmail enterTopic(String topic){
        log.info("Enter Topic: " + topic);
        driver.findElement(topicLocator).sendKeys(topic);
        return this;
    }
    public DraftEmail enterEmailContent(String emailContent){
        log.info("Enter email content");
        driver.findElement(emailContentLocator).click();
        driver.findElement(emailContentLocator).sendKeys(emailContent);
        return this;
    }
    public DraftEmail sendMessage(){
        log.info("Send the email");
        driver.findElement(sendButtonLocator).click();
        waitLoading();
        return this;
    }
    public DraftEmail saveAndCloseMessage(){
        log.info("Save and close the email to the draft folder");
        driver.findElement(closeButtonLocator).click();
        return this;
    }
    public DraftEmail deleteDraft(){
        log.info("Delete draft email");
        driver.findElement(deleteDraftButtonLocator).sendKeys(Keys.ENTER); //click() method doesn't work
        return this;
    }
    public String getAddressee(){
        return driver.findElement(addresseeLocator).getAttribute("email");
    }
    public String getEmailTopic(){
        return driver.findElement(topicLocator).getText();
    }
    public String getEmailContent(){
        return driver.findElement(emailContentLocator).getText();
    }
}
