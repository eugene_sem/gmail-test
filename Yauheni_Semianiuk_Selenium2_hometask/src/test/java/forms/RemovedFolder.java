package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RemovedFolder extends MainPage {
    private final WebDriver driver;
    private static By emptyTrashLocator = By.id(":106");
    public RemovedFolder(){
        super(webDriver, trashLocator);
        this.driver = webDriver;
    }
}
