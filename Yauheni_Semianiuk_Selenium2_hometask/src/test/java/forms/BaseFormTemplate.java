package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

public class BaseFormTemplate {

    private By _locator;
    static protected WebDriver webDriver;
    private static final java.util.logging.Logger log = java.util.logging.Logger.getLogger(EnterEmailPage.class.getName());

    public static void initialiseWebDriver(){
        log.info("Initialise WebDriver");
        System.setProperty("webdriver.gecko.driver", "src/test/java/resources/geckodriver");
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
    }

    public BaseFormTemplate(WebDriver webDriver, By locator)
    {
        _locator=locator;
        WebDriverWait wait = new WebDriverWait(webDriver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated((_locator)));
        wait.until(ExpectedConditions.elementToBeClickable(_locator));
    }
    public static WebDriver getWebDriver(){
        return  webDriver;
    }
}
