package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;


public class NewDraftEmail extends DraftEmail {
    private static final Logger log = Logger.getLogger(NewDraftEmail.class.getName());
    private static By addresseeLocator = By.name("to");
    private static By topicLocator = By.name("subjectbox");

    private final WebDriver driver;
    public NewDraftEmail(){
        super();
        this.driver = webDriver;
    }
    public DraftEmail enterAddressee(String addressee) {
        log.info("Enter Addressee: " + addressee);
        driver.findElement(addresseeLocator).sendKeys(addressee);
        return this;
    }
    public DraftEmail enterTopic(String topic) {
        log.info("Enter Topic: " + topic);
        driver.findElement(topicLocator).sendKeys(topic);
        return this;
    }
}
