package forms;

import java.util.Random;

public class RandomGenerator {
    static public String makeRandomString(int length){
        char[] chars = new char[length];
        Random random = new Random();
        String allCharsString = "abcdefghijklmnopqrstuvwxyzABCEDFGHIJKLMNOPQRSTUVWXYZ1234567890";
        char[] allChars = allCharsString.toCharArray();
        for (int i = 0; i < length; i++){
            chars[i] = allChars[random.nextInt(allChars.length)];
        }
        return new String(chars);
    }
}
