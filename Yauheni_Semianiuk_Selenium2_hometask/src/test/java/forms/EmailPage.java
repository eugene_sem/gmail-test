package forms;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

public class EmailPage extends MainPage{
    private static final Logger log = Logger.getLogger(EmailPage.class.getName());
    private static By emailFromLocator = By.cssSelector("h3 > span[email]");
    private By emailTopicLocator = By.cssSelector("div > h2[class][tabindex='-1']");
    private By emailContent = By.cssSelector("div > div > div[dir]");
    private By emailToLocator = By.cssSelector("td > div > span > span");
    private By deleteEmailLocator = By.cssSelector("div[gh='mtb'] > div > div > div[act='10']");
    private By deleteEmailConfirmLocator = By.cssSelector("div[gh='mtb'] > div > div >div[act='17']");
    private final WebDriver driver;
    public EmailPage(){
        super(webDriver, emailFromLocator);
        this.driver = webDriver;
    }
    public EmailPage deleteEmail(){
        log.info("Delete Email");
        driver.findElement(deleteEmailLocator).click();
        return this;
    }
    public RemovedFolder confirmEmailDeletion(){
        log.info("Confirm email deletion");
        driver.findElement(deleteEmailConfirmLocator).click();
        return new RemovedFolder();
    }
    public String getEmailFrom(){
        return driver.findElement(emailFromLocator).getAttribute("email");
    }
    public String getEmailTo(){
        return driver.findElement(emailToLocator).getAttribute("email");
    }
    public String getEmailTopic(){
        return driver.findElement(emailTopicLocator).getText();
    }
    public String getEmailContent(){
        return driver.findElement(emailContent).getText();
    }

}
