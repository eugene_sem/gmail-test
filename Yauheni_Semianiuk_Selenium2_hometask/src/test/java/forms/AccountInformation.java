package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

public class AccountInformation extends MainPage{
    private static final Logger log = Logger.getLogger(AccountInformation.class.getName());
    private static By logOutButtonLocator = By.xpath("//a[contains(@href, 'Logout')]");
    private static By myAccountLocator = By.cssSelector("div > div > div > div[aria-hidden='false']");
    private static By addAnotherAccountLocator = By.xpath("//a[contains(@href, 'AddSession')]");

    private final WebDriver driver;

    public AccountInformation() {
        super(webDriver, addAnotherAccountLocator);
        this.driver = webDriver;
    }
    public EnterPasswordPage logOut(){
        log.info("Log out");
        driver.findElement(logOutButtonLocator).click();
        return new EnterPasswordPage();
    }
    public void addAnotherAccount(){
        log.info("Add another account");
        driver.findElement(addAnotherAccountLocator).click();
    }
}
