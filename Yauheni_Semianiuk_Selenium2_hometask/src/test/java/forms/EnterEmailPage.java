package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.logging.Logger;


public class EnterEmailPage extends BaseFormTemplate {
    private static By emailLocator = By.id("identifierId");
    private By nextButton = By.id("identifierNext");
    private static final Logger log = Logger.getLogger(EnterEmailPage.class.getName());

    private final WebDriver driver;

    public EnterEmailPage(){
        super(getWebDriver(), emailLocator);
        this.driver = webDriver;
    }

    public EnterEmailPage typeUsername(String username) {
        log.info("Type username: " + username);
        driver.findElement(emailLocator).sendKeys(username);
        return this;
    }

    public EnterPasswordPage submitEmail(){
        log.info("Submit username");
        driver.findElement(nextButton).click();
        return new EnterPasswordPage();
    }

}
