package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.logging.Logger;

public class EnterPasswordPage extends BaseFormTemplate {

    private static By passwordLocator = By.xpath("//input[@type='password']");
    private By signInButton = By.id("passwordNext");
    private By anotherAccountLocator = By.id("account-chooser-link");
    private static final Logger log = Logger.getLogger(EnterPasswordPage.class.getName());

    private final WebDriver driver;
    public EnterPasswordPage(){
        super(webDriver, passwordLocator);
        this.driver = webDriver;
    }
    public EnterPasswordPage typePassword(String password){
        log.info("Type password: " + password);
        driver.findElement(passwordLocator).sendKeys(password);
        return this;
    }
    public InboxFolder signIn(){
        log.info("Sign In to the account");
        driver.findElement(signInButton).click();
        try{
            return new InboxFolder();
        } catch (Exception e){
           throw new IllegalStateException("Login is failed!");
        }
    }
    public ChooseAccountPage signInWithDifferentAccount(){
        log.info("Sign In with different account");
        driver.findElement(anotherAccountLocator).click();
        return new ChooseAccountPage();
    }


}
